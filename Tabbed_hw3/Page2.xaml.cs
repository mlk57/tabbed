﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed_hw3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
        }
        private async void OnButtonClicked2(object sender, EventArgs e)
        {
            await DisplayAlert("So sad", "You are wrong", "OK");
        }
        private async void OnButtonClickedNo2(object sender, EventArgs e)
        {
            await DisplayAlert("Yes", "You are right", "COOL");
        }
    }
}
