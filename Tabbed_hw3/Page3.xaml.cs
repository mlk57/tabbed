﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed_hw3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }
        private async void OnButtonClickedImage(object sender, EventArgs e)
        {
            await DisplayAlert("Yes ", "Good job", "OK");
        }
        private async void Nope(object sender, EventArgs e)
        {
            await DisplayAlert("NOPE", "NOPE", "NOPE");
        }
        protected override void OnAppearing()
        {
            DisplayAlert("Info", "Albert Einstein was born in Ulm, in the Kingdom of Württemberg in the German Empire, on 14 March 1879", "Ok");
        }
    }
}