﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed_hw3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page4 : ContentPage
    {
        public Page4()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            DisplayAlert("List", "list of countries starting with A and B", "Ok");
        }
        protected override void OnDisappearing()
        {
            DisplayAlert("List", "It was the list of countries starting with A and B", "Ok");
        }
    }
}