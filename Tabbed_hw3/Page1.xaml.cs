﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed_hw3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }
        private void submit_Clicked(object sender, EventArgs e) // get input of user and chck if the answer is right
        {
            var nameValue = name.Text;
            if (name.Text == "15")
            {
                DisplayAlert("Good job", "Are you a genius ?????", "YES");
            }
            else
            {
                DisplayAlert("Nope", "You are not a genius", "YES");
            }
        }
        protected override void OnAppearing()
        {
            DisplayAlert("1 page", "This is the first page", "Ok");
        }
        protected override void OnDisappearing()
        {
            DisplayAlert("1 page", "It was the first page", "Ok");
        }
    }
}